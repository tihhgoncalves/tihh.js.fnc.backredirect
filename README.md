# tihh_backRedirect() - Função de JavaScript
Função de JavaScript que manipula o botão "voltar" do navegador. 

### Instalação
Para utilizar esse script, utilize o seguinte comando:

```
  bower install https://bitbucket.org/tihhgoncalves/tihh.js.fnc.backredirect.git --save
```
P.S.: Caso prefira, você pode fazer o download do projeto manualmente.

### Exemplo

```
  <script>
  
  tihh_backRedirect('http://www.tiago.art.br');
  
  /**  
    Após abrir a página com esse script, clique no 'voltar' do seu navegador.
    Se tudo der certo, sua página irá para o www.tiago.art.br 
    como se você tivesse acessado essa página anterioremente)
   */
   
  </script>
```

### Autor

Esse script de PHP foi desenvolvido por Tihh Gonçalves (tiago@taticadesucesso.com.br).

Mais informações: www.taticadesucesso.com.br
